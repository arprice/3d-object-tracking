/**
 * \file tutorial.cpp
 * \brief
 *
 * \author Andrew Price
 * \date 6 25, 2014
 *
 * \copyright
 *
 * Copyright (c) 2014, Georgia Tech Research Corporation
 * All rights reserved.
 *
 * This file is provided under the following "BSD-style" License:
 * Redistribution and use in source and binary forms, with or
 * without modification, are permitted provided that the following
 * conditions are met:
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <vector>
#include <iostream>

//#define GLEW_STATIC
#include <GL/glew.h>
#include <GL/glut.h>

#include "RenderMesh.h"
#include "RenderMatches.h"
#include "CommonDefinitions.h"
#include "RayProjector.h"

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/flann/flann.hpp>

RenderMesh* rm;

float Z_NEAR = 1.0f;//0.3750f;
float Z_FAR = 10.0f;//0.60f;

int MAIN_WIN_ID = 0;
int HELPER_WIN_ID = 0;

cv::Mat edgeImg;

cv::Mat colorImg = cv::Mat(cv::Size(RENDER_SIZE_X, RENDER_SIZE_Y), CV_8UC3);
cv::Mat depthImg = cv::Mat(cv::Size(RENDER_SIZE_X, RENDER_SIZE_Y), CV_32FC1);
cv::Mat coordImg = cv::Mat(cv::Size(RENDER_SIZE_X, RENDER_SIZE_Y), CV_64FC3);

std::vector<cv::Point2i> realEdgeIndices;
std::vector<cv::Point2i> synthEdgeIndices;
cv::Mat indices;

//cv::flann::Index* flann_index;

Eigen::Isometry3f objectFrame = Eigen::Isometry3f::Identity();

const float cameraConversion[16] = {1,0,0,0,
									0,-1,0,0,
									0,0,-1,0,
									0,0,0,1};
double modelMatrix[16] = {1,0,0,0,
						  0,1,0,0,
						  0,0,1,0,
						  0,0,0,1};
double projMatrix[16];
GLint viewport[4];

//Eigen::Vector3f inspector = Eigen::Vector3f::Zero();
Eigen::Matrix3f rot = Eigen::AngleAxisf(0.01f, Eigen::Vector3f::UnitY()).matrix();
float angle = 0.0f;
static GLuint vao;

OrbitParams op;
RayProjector* rp = RayProjector::Instance();

//#define USE_CV_COORDINATE_CONVENTION

void loadEdgeImage()
{
	cv::Mat imgRead = cv::imread(cannyFilename);
	cv::Mat imgFilt, imgDisp;

	int neighborhood = 1 + std::min(imgRead.rows, imgRead.cols) / 300;
	int bilat = 3;
	cv::bilateralFilter(imgRead, imgFilt, -1, bilat, neighborhood);
	cv::blur(imgFilt, imgFilt, cv::Size(3, 3));
	int thresh = 20;
	cv::Canny(imgFilt, edgeImg, thresh, thresh * 3);


	int pixelCount = 0;
	const int pixelRatio = 1;
	for (int j = 0; j < edgeImg.rows; ++j)
	{
		for (int i = 0; i < edgeImg.cols; ++i)
		{
			bool c = edgeImg.ptr(j)[i];
			if (c)
			{
				if (pixelCount % pixelRatio == 0)
				{
					realEdgeIndices.push_back(cv::Point2i(i, j));
				}
				++pixelCount;
			}
		}
	}

//	cv::Mat reals; cv::Mat(realEdgeIndices).convertTo(reals, CV_32FC1);
//	reals = reals.reshape(1,realEdgeIndices.size());

	//flann_index = new cv::flann::Index(reals, cv::flann::KDTreeIndexParams(1)); // cvflann::FLANN_DIST_EUCLIDEAN
}

void getViewMatrices()
{
	//glGetDoublev(GL_MODELVIEW_MATRIX,modelMatrix);
	glGetDoublev(GL_PROJECTION_MATRIX,projMatrix);
	glGetIntegerv(GL_VIEWPORT,viewport);
}

/**
 * @brief Computes ray from origin through pixel
 * @param u Image coordinate column
 * @param v Image coordinate row
 * @return Unnormalized ray with z=1
 *
 * Image coordinates:
 *      u->
 * (0,0)____________
 *   v |          |
 *   | |          |
 *   V |          |
 *     |          |
 *     |__________|
 *
 * CG coordinates:
 *    y
 *    |
 *    |___ x
 *   /
 *  z
 *
 * CV coordinates:
 *     z
 *	  /___ x
 *	  |
 *	  |
 *    y
 */
//inline Eigen::Vector3f rayThroughPixel(const int u, const int v)
//{
//#ifdef USE_CV_COORDINATE_CONVENTION
//	return Eigen::Vector3f(tan((u - RENDER_SIZE_X / 2)*FOV_DELTA_X),
//						   tan((v - RENDER_SIZE_Y / 2)*FOV_DELTA_Y),
//						   1);
//#else
//	return Eigen::Vector3f(tan((u - RENDER_SIZE_X / 2)*FOV_DELTA_X),
//						   tan((RENDER_SIZE_Y / 2 - v)*FOV_DELTA_Y),
//						   -1).normalized();
////	Eigen::Vector3d ray;
////	gluUnProject(u, v, 1,
////				 modelMatrix, projMatrix, viewport,
////				 &ray[0], &ray[1], &ray[2]);
////	return ray.cast<float>().normalized();
//#endif
//}

void doMatch(bool doAnimate = true)
{
	assert(synthEdgeIndices.size() > 0);
	cv::Mat reals; cv::Mat(realEdgeIndices).convertTo(reals, CV_32FC1);
	reals = reals.reshape(1,realEdgeIndices.size());

	cv::Mat synth; cv::Mat(synthEdgeIndices).convertTo(synth, CV_32FC1);
	synth = synth.reshape(1,synthEdgeIndices.size());

	cv::flann::Index flann_index = cv::flann::Index(synth, cv::flann::KDTreeIndexParams(1)); // cvflann::FLANN_DIST_EUCLIDEAN

	indices = cv::Mat(realEdgeIndices.size(), 1, CV_32S); //std::vector<int> indices;
	cv::Mat distances(realEdgeIndices.size(), 1, CV_32F); //std::vector<float> distances;
	flann_index.knnSearch(reals, indices, distances, 1);

	// Distance vector D from Point P to Vector V with Origin O
	//
	//      P
	//     /|
	//    / | D
	//   /  v
	//  O---X----->V
	//
	//  X = (P dot V)*V
	//  D = (P dot V)*V - P

	Eigen::Vector3f modelForce = Eigen::Vector3f::Zero();
	Eigen::Vector3f modelTorque = Eigen::Vector3f::Zero();

	float totalErr = 0;

	for (int i = 0; i < indices.rows; ++i)
	{
		const int j = *indices.ptr<int>(i);

		const cv::Point2i& ptA = synthEdgeIndices[j];
		const cv::Point2i& ptB = realEdgeIndices[i];

		double* rowPtr = coordImg.ptr<double>(ptA.y);

		Eigen::Vector3f p = Eigen::Vector3f(rowPtr[ptA.x*3 + 0],
											rowPtr[ptA.x*3 + 1],
											rowPtr[ptA.x*3 + 2]);
		Eigen::Vector3f v = RayProjector::rayThroughPixel(ptB.x, ptB.y);

		assert(p.z() != 0.0 || p.x() != 0.0 || p.y() != 0.0);

		Eigen::Vector3f d = p.dot(v)*v - p;

		//Eigen::Vector3f d = Eigen::Vector3f(ptB.x-ptA.x, -(ptB.y-ptA.y), 0);

		totalErr += d.norm();

		//d /= d.squaredNorm() + 0.0001; // closer points are stronger up to a point
		d /= d.norm() + 0.0001;

		modelForce += d / (d.squaredNorm() + 0.0001);
		modelTorque += (p-objectFrame.translation()).cross(d / (d.norm() + 0.000001)); // Moment is (r cross f)

		assert(modelForce.x() == modelForce.x());
	}

	modelForce /= indices.rows;
	modelTorque /= indices.rows;

//	std::cout << "Force: " << modelForce.transpose() << std::endl;
//	std::cout << "Moment: " << modelTorque.transpose() << std::endl;

	Eigen::Vector3f objectLoc = objectFrame.translation();
	objectFrame.translation() = Eigen::Vector3f::Zero();
	objectFrame.rotate(Eigen::AngleAxisf(modelTorque.norm() * 0.05, modelTorque.normalized()));
	objectFrame.translation() = objectLoc + modelForce * 0.005;

//	objectFrame.translation() += modelForce * 0.0001;

	if (doAnimate)
	{
		cv::Mat imgQueries = cv::Mat(edgeImg.size(), CV_8UC1, cv::Scalar(0));
		for (cv::Point2i pt : synthEdgeIndices)
		{
			cv::circle(imgQueries, pt, 2, cv::Scalar(255));
		}

		cv::Mat imgMatches = cv::Mat(edgeImg.size(), CV_8UC1, cv::Scalar(0));
		for (int i = 0; i < indices.rows; ++i)
		{
			const int j = *indices.ptr<int>(i);
			const cv::Point2i& ptA = synthEdgeIndices[j];
			const cv::Point2i& ptB = realEdgeIndices[i];

			cv::line(imgMatches, ptA, ptB, cv::Scalar(255));
		}

		cv::Mat disp;
		std::vector<cv::Mat> channels(3);
		channels[0] = edgeImg;
		channels[1] = imgMatches;
		channels[2] = imgQueries;
		cv::merge(channels, disp);


		cv::imshow("Matches", disp);
		//cv::waitKey(0);
	}
}

void saveScene()
{
	//use fast 4-byte alignment (default anyway) if possible
	glPixelStorei(GL_PACK_ALIGNMENT, (depthImg.step & 3) ? 1 : 4);

	//set length of one complete row in destination data (doesn't need to equal img.cols)
	glPixelStorei(GL_PACK_ROW_LENGTH, depthImg.step/depthImg.elemSize());

	glReadPixels(0, 0, depthImg.cols, depthImg.rows, GL_DEPTH_COMPONENT, GL_FLOAT, (void*)depthImg.data);

	//glPixelStorei(GL_PACK_ALIGNMENT, (colorImg.step & 3) ? 1 : 4);
	//glPixelStorei(GL_PACK_ROW_LENGTH, colorImg.step/colorImg.elemSize());
	glReadPixels(0, 0, colorImg.cols, colorImg.rows, GL_BGR, GL_UNSIGNED_BYTE, (void*)colorImg.data);


	double depth_bias, depth_scale;
	glGetDoublev(GL_DEPTH_BIAS,  &depth_bias);  // Returns 0.0
	glGetDoublev(GL_DEPTH_SCALE, &depth_scale); // Returns 1.0

	assert(0.0 == depth_bias);


	int queryCount = 0;
	const int queryRatio = 1;
	synthEdgeIndices.clear();
	for (int j = 0; j < depthImg.rows; ++j)
	{
		double* rowPtr = coordImg.ptr<double>(j);
		for (int i = 0; i < depthImg.cols; ++i)
		{
			uchar c = colorImg.ptr(j)[i*3];

//			double z = depthImg.ptr<float>(j)[i];

//			gluUnProject(i, j, z,
//						 modelMatrix, projMatrix, viewport,
//						 rowPtr + (i*3 + 0),
//						 rowPtr + (i*3 + 1),
//						 rowPtr + (i*3 + 2));

			if (255 == c)
			{
				double z = depthImg.ptr<float>(j)[i];

				gluUnProject(i, j, z,
							 modelMatrix, projMatrix, viewport,
							 rowPtr + (i*3 + 0),
							 rowPtr + (i*3 + 1),
							 rowPtr + (i*3 + 2));

				if (queryCount % queryRatio == 0)
				{
//					double xd = rowPtr[i*3 + 0];
//					double yd = rowPtr[i*3 + 1];
//					double zd = rowPtr[i*3 + 2];

					// Need to reverse synthIndices, since it won't be flipped later...
					synthEdgeIndices.push_back(cv::Point2i(i, (depthImg.rows-1)-j));
				}
				++queryCount;
			}
			else
			{
				rowPtr[i*3 + 0] = 0.0;
				rowPtr[i*3 + 1] = 0.0;
				rowPtr[i*3 + 2] = 0.0;
			}
		}
	}

	cv::flip(depthImg, depthImg, 0);
	cv::flip(colorImg, colorImg, 0);
	cv::flip(coordImg, coordImg, 0);

	//double minVal = 0, maxVal = 0;
	//cv::minMaxLoc(coordImg, &minVal);
	//coordImg -= minVal;
	//cv::minMaxLoc(coordImg, &minVal, &maxVal);
	//coordImg /= maxVal;
	//coordImg = cv::abs(coordImg);
	//coordImg = (coordImg - 1.0f) * -1.0f;

	cv::Mat dispImg = (depthImg - 1.0f) * -1.0f;

	bool doAnimate = true;
	doMatch(doAnimate);

	if (doAnimate)
	{
		//std::cout << depthImg << std::endl;
		cv::imshow("Depth", dispImg);
		cv::waitKey(1);
	}
}

void changeSize(int w, int h)
{

	// Prevent a divide by zero, when window is too short
	// (you cant make a window of zero width).
	if (h == 0)
		h = 1;

	float ratio =  w * 1.0 / h;

	// Use the Projection Matrix
	glMatrixMode(GL_PROJECTION);

	// Reset Matrix
	glLoadIdentity();

	// Set the viewport to be the entire window
	glViewport(0, 0, w, h);

	// Set the correct perspective.
	//gluPerspective(45.0f, ratio, 0.1f, 100.0f);
	gluPerspective(FOV_DEG, 1.0f, Z_NEAR, Z_FAR);

	// Get Back to the Modelview
	glMatrixMode(GL_MODELVIEW);
}

void renderScene(void)
{
	glutSetWindow(MAIN_WIN_ID);
	// Clear Color and Depth Buffers
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Reset transformations
	glLoadIdentity();
	//glLoadMatrixf(cameraConversion);

	// Set the camera
	Eigen::Matrix4f objectMat = objectFrame.matrix(); // objectFrame.inverse().matrix();

	glPushMatrix();
	glMultMatrixf(objectMat.data());
	rm->RenderArray();
	glPopMatrix();

	GLint err = glGetError();
	assert_msg(GL_NO_ERROR == err, gluErrorString(err));

	getViewMatrices();

	saveScene();

	glutSetWindow(HELPER_WIN_ID);
	draw3DMatches(realEdgeIndices, synthEdgeIndices, coordImg, indices, objectFrame, op);

	glutSwapBuffers();
}

void processSpecialKeys(int key, int x, int y)
{
	const float ANGULAR_STEP = 0.1;
	switch(key)
	{
	case GLUT_KEY_UP:
		op.theta -= ANGULAR_STEP;
		break;
	case GLUT_KEY_DOWN:
		op.theta += ANGULAR_STEP;
		break;
	case GLUT_KEY_LEFT:
		op.phi -= ANGULAR_STEP;
		break;
	case GLUT_KEY_RIGHT:
		op.phi += ANGULAR_STEP;
		break;
	case GLUT_KEY_PAGE_UP:
		op.rho -= ANGULAR_STEP;
		break;
	case GLUT_KEY_PAGE_DOWN:
		op.rho += ANGULAR_STEP;
		break;
	}
}

//Main program

int main(int argc, char **argv)
{
	glutInit(&argc, argv);

	/*Setting up  The Display
	/    -RGB color model + Alpha Channel = GLUT_RGBA
	*/
	glutInitDisplayMode(GLUT_RGBA|GLUT_SINGLE|GLUT_DEPTH|GLUT_STENCIL);

	//Configure Window Postion
	glutInitWindowPosition(50, 25);

	//Configure Window Size
	glutInitWindowSize(RENDER_SIZE_X, RENDER_SIZE_Y);

	//Create Window
	MAIN_WIN_ID = glutCreateWindow("Hello OpenGL");

	glewExperimental = GL_TRUE;
	glewInit();

	int major, minor;
	glGetIntegerv(GL_MAJOR_VERSION, &major);
	glGetIntegerv(GL_MINOR_VERSION, &minor);
	std::cout << glGetString(GL_VENDOR) << " OpenGL Version " << major << "." << minor << std::endl;

	glEnable(GL_DEPTH_TEST); // Z-buffering
	glDepthFunc(GL_LESS);
	glCullFace(GL_BACK); // Face culling removes faces pointing away from you
	glEnable(GL_CULL_FACE);

	//rm = new RenderMesh("/home/arprice/Dropbox/GTRI/robots/KR5sixxR650WP_description/meshes/bicep.STL");
	rm = new RenderMesh(modelFilename);
	loadEdgeImage();

	// Zoom extents
	float x,  y,  z,  r;
	rm->m_mesh.boundingSphere(x,  y,  z,  r);
	float startDistance = r / sin(FOV/2.0) * 1.1f;
	Eigen::Vector3f start = Eigen::Vector3f(-x, -y, -startDistance);
//	assert_msg(fabs(startDistance) > Z_NEAR, "start: " << startDistance);
//	assert_msg(fabs(startDistance) < Z_FAR, "start: " << startDistance);

	float xMin,  yMin,  zMin,  xMax,  yMax,  zMax;
	rm->m_mesh.boundingBox(xMin,  yMin,  zMin,  xMax,  yMax,  zMax);
	zMin += start.z();
	zMax += start.z();

	Z_NEAR = fabs(zMin * 0.450f);
	Z_FAR = fabs(zMax * 1.50f);

//	assert_msg(fabs(zMin) > Z_NEAR, "start: " << zMin);
//	assert_msg(fabs(zMin) < Z_FAR, "start: " << zMin);
//	assert_msg(fabs(zMax) > Z_NEAR, "start: " << zMax);
//	assert_msg(fabs(zMax) < Z_FAR, "start: " << zMax);

	objectFrame.translate(start);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(FOV_DEG, 1.0f, Z_NEAR, Z_FAR);
	glMatrixMode(GL_MODELVIEW);

	getViewMatrices();

	std::cout << RayProjector::rayThroughPixel(RENDER_SIZE_X/2,RENDER_SIZE_Y/2).normalized().transpose() << std::endl;
	std::cout << RayProjector::rayThroughPixel(0,0).normalized().transpose() << std::endl;
	std::cout << RayProjector::rayThroughPixel(3*RENDER_SIZE_X/4,1*RENDER_SIZE_Y/4).normalized().transpose() << std::endl;

	assert (GL_NO_ERROR == glGetError());

	cv::namedWindow("Depth", cv::WINDOW_NORMAL);
	cv::moveWindow("Depth", 50 + RENDER_SIZE_X, 25);
	cv::resizeWindow("Depth", RENDER_SIZE_X, RENDER_SIZE_Y);
	cv::namedWindow("Matches", cv::WINDOW_NORMAL);
	cv::moveWindow("Matches", 50 + RENDER_SIZE_X*2, 25);
	cv::resizeWindow("Matches", RENDER_SIZE_X, RENDER_SIZE_Y);

	//Call to the drawing function
	glutDisplayFunc(renderScene);
	glutReshapeFunc(changeSize);
	glutIdleFunc(renderScene);

	assert (GL_NO_ERROR == glGetError());

	glutInitWindowPosition(50, 25 + RENDER_SIZE_Y + 25);
	glutInitWindowSize(RENDER_SIZE_X*2.5, RENDER_SIZE_Y*2.5);
	HELPER_WIN_ID = glutCreateWindow("3D Matches");
	glutDisplayFunc(renderScene);
	glutReshapeFunc(changeSize);
	glutSpecialFunc(processSpecialKeys);
	//glutIdleFunc(renderMatches);


	// Loop require by OpenGL
	glutMainLoop();
	return 0;
}
