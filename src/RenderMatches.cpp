/**
 * \file RenderMatches.cpp
 * \brief
 *
 * \author Andrew Price
 * \date 7 4, 2014
 *
 * \copyright
 *
 * Copyright (c) 2014, Georgia Tech Research Corporation
 * All rights reserved.
 *
 * This file is provided under the following "BSD-style" License:
 * Redistribution and use in source and binary forms, with or
 * without modification, are permitted provided that the following
 * conditions are met:
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "RenderMatches.h"
#include "RayProjector.h"

#include <GL/glew.h>
#include <GL/glut.h>

void renderMatches()
{
	glutSwapBuffers();
}

void drawCoordinateSystem(const Eigen::Isometry3f& origin, const float scale = 1.0, const float thickness = 2.0)
{
	Eigen::Vector3f t = origin.translation();
	Eigen::Matrix3f R = origin.rotation().matrix()*scale;
	glLineWidth(thickness);
	glColor3f(1, 0, 0);
	glBegin(GL_LINES);
	glVertex3f(t.x(), t.y(), t.z());
	glVertex3f(t.x()+R.col(0)[0], t.y()+R.col(0)[1], t.z()+R.col(0)[2]);
	glEnd();
	glColor3f(0, 1, 0);
	glBegin(GL_LINES);
	glVertex3f(t.x(), t.y(), t.z());
	glVertex3f(t.x()+R.col(1)[0], t.y()+R.col(1)[1], t.z()+R.col(1)[2]);
	glEnd();
	glColor3f(0, 0, 1);
	glBegin(GL_LINES);
	glVertex3f(t.x(), t.y(), t.z());
	glVertex3f(t.x()+R.col(2)[0], t.y()+R.col(2)[1], t.z()+R.col(2)[2]);
	glEnd();
}

void draw3DMatches(const std::vector<cv::Point2i>& realEdgeIndices,
				   const std::vector<cv::Point2i>& synthEdgeIndices,
				   const cv::Mat& coordImg,
				   const cv::Mat& matchIndices,
				   const Eigen::Isometry3f& objectOrigin,
				   const OrbitParams op)
{
	// Clear Color and Depth Buffers
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Reset transformations
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(30.0f, 1.0f, 0.1f, 100.0f);

	Eigen::Vector3f cameraOrigin = objectOrigin.translation() + op.toCGCartesian();

	gluLookAt(cameraOrigin.x(), cameraOrigin.y(), cameraOrigin.z(),
			  objectOrigin.translation().x(), objectOrigin.translation().y(), objectOrigin.translation().z(),
			  0, 1, 0);
	glMatrixMode(GL_MODELVIEW);

	// Draw Axes
	Eigen::Isometry3f glOrigin = Eigen::Isometry3f::Identity();
	drawCoordinateSystem(glOrigin);
	drawCoordinateSystem(objectOrigin, 0.1);

	Eigen::Vector3f modelForce = Eigen::Vector3f::Zero();
	Eigen::Vector3f modelTorque = Eigen::Vector3f::Zero();

	glPointSize(3.0f);
	glLineWidth(1.0f);

	for (int i = 0; i < matchIndices.rows; ++i)
	{
		const int j = *matchIndices.ptr<int>(i);

		const cv::Point2i& ptA = synthEdgeIndices[j];
		const cv::Point2i& ptB = realEdgeIndices[i];

		const double* rowPtr = coordImg.ptr<double>(ptA.y);

		Eigen::Vector3f p = Eigen::Vector3f(rowPtr[ptA.x*3 + 0],
											rowPtr[ptA.x*3 + 1],
											rowPtr[ptA.x*3 + 2]);
		Eigen::Vector3f v = RayProjector::rayThroughPixel(ptB.x, ptB.y);

//		assert(p.z() != 0.0 || p.x() != 0.0 || p.y() != 0.0);

		Eigen::Vector3f d = p.dot(v)*v - p;


//		Eigen::Vector3f d = Eigen::Vector3f(ptB.x-ptA.x, -(ptB.y-ptA.y), 0);

		// Draw the 3D computed point
		glColor3f(1, 0, 0);
		glBegin(GL_POINTS);
		glVertex3f(p.x(), p.y(), p.z());
		glEnd();

		// Draw the 3D ray
		v *= objectOrigin.translation().norm();
		glColor3f(0, 0, 1);
		glBegin(GL_LINES);
		glVertex3f(0, 0, 0);
		glVertex3f(v.x(), v.y(), v.z());
		glEnd();

		// Draw the 3D Match
		glColor3f(0, 1, 0);
		glBegin(GL_LINES);
		glVertex3f(p.x(), p.y(), p.z());
		glVertex3f(p.x()+d.x(), p.y()+d.y(), p.z()+d.z());
		glEnd();

//		d /= d.squaredNorm() + 0.0001; // closer points are stronger up to a point
		d /= d.norm() + 0.0001;

		modelForce += d;
		modelTorque += (p-objectOrigin.translation()).cross(d); // Moment is (r cross f)
	}

	modelForce /= matchIndices.rows;
	modelTorque /= matchIndices.rows;

	glColor3f(1, 0, 1);
	glBegin(GL_LINES);
	glVertex3f(objectOrigin.translation().x(), objectOrigin.translation().y(), objectOrigin.translation().z());
	glVertex3f(objectOrigin.translation().x()+modelForce.x(),
			   objectOrigin.translation().y()+modelForce.y(),
			   objectOrigin.translation().z()+modelForce.z());
	glEnd();

	glColor3f(0, 1, 1);
	glBegin(GL_LINES);
	glVertex3f(objectOrigin.translation().x(), objectOrigin.translation().y(), objectOrigin.translation().z());
	glVertex3f(objectOrigin.translation().x()+modelTorque.x(),
			   objectOrigin.translation().y()+modelTorque.y(),
			   objectOrigin.translation().z()+modelTorque.z());
	glEnd();
}
