/**
 * \file RayProjector.cpp
 * \brief
 *
 * \author Andrew Price
 * \date 7 7, 2014
 *
 * \copyright
 *
 * Copyright (c) 2014, Georgia Tech Research Corporation
 * All rights reserved.
 *
 * This file is provided under the following "BSD-style" License:
 * Redistribution and use in source and binary forms, with or
 * without modification, are permitted provided that the following
 * conditions are met:
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "RayProjector.h"

RayProjector* RayProjector::mpInstance = NULL;
Eigen::Vector3f RayProjector::rays[RENDER_SIZE_Y][RENDER_SIZE_X];

RayProjector* RayProjector::Instance()
{
	if (!mpInstance)
	{
		mpInstance = new RayProjector();
	}
	return mpInstance;
}

RayProjector::RayProjector()
{
	for (int v = 0; v < RENDER_SIZE_Y; ++v)
	{
		for (int u = 0; u < RENDER_SIZE_Y; ++u)
		{
#ifdef USE_CV_COORDINATE_CONVENTION
			rays[v][u] =  Eigen::Vector3f(tan((u - RENDER_SIZE_X / 2)*FOV_DELTA_X),
										  tan((v - RENDER_SIZE_Y / 2)*FOV_DELTA_Y),
										  1).normalized();
#else
			rays[v][u] = Eigen::Vector3f(tan((u - RENDER_SIZE_X / 2)*FOV_DELTA_X),
										 tan((RENDER_SIZE_Y / 2 - v)*FOV_DELTA_Y),
										 -1).normalized();
//			Eigen::Vector3d ray;
//			gluUnProject(u, v, 1,
//			             modelMatrix, projMatrix, viewport,
//			             &ray[0], &ray[1], &ray[2]);
//			rays[v][u] = ray.cast<float>().normalized();
#endif
		}
	}
}
