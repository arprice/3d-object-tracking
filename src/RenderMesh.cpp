/**
 * \file RenderMesh.cpp
 * \brief
 *
 * \author Andrew Price
 * \date 6 26, 2014
 *
 * \copyright
 *
 * Copyright (c) 2014, Georgia Tech Research Corporation
 * All rights reserved.
 *
 * This file is provided under the following "BSD-style" License:
 * Redistribution and use in source and binary forms, with or
 * without modification, are permitted provided that the following
 * conditions are met:
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "RenderMesh.h"

#include <iostream>
#include <algorithm>
#include <math.h>

#include <assimp/mesh.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <assimp/Importer.hpp>


#define GLEW_STATIC
#include <GL/glew.h>
#include <GL/glut.h>

const float SCALE = 1.0;
typedef std::pair<uint, uint> IdxPair;

struct compareVectors
{
	bool operator() (const aiVector3D& s, const aiVector3D& r)
	{
		if (s.Length() < r.Length())
		{
			return true;
		}
		else if (s.Length() == r.Length())
		{
			if (s.x + s.y + s.z < r.x + r.y + r.z)
			{
				return true;
			}
			else if (s.x + s.y + s.z == r.x + r.y + r.z)
			{
				if (s.x < r.x)
				{
					return true;
				}
				else if(s.x == r.x)
				{
					if (s.y < r.y)
					{
						return true;
					}
					else if(s.y == r.y)
					{
						return s.z < r.z;
					}
					else
					{
						return false;
					}
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		return false;

	}
};

typedef std::set<aiVector3D, compareVectors> VectorSet;


uint getIteratorIndex(const std::vector<VectorSet::iterator>& vec, const VectorSet::iterator& iter)
{
	for (uint i = 0; i < vec.size(); ++i)
	{
		if (vec[i] == iter)
		{
			return i;
		}
	}

	std::cerr << "ERROR: Target not found." << std::endl;

	return 0;
}

bool meshAItoAP(const aiScene* inScene, Mesh* outMesh)
{
	// Find a triangle mesh
	int triMeshIdx = 0;
	for (int i = 0; i < inScene->mNumMeshes; i++)
	{
		if (inScene->mMeshes[i]->HasFaces() && inScene->mMeshes[i]->mFaces[0].mNumIndices == 3)
		{
			triMeshIdx = i;
		}
	}

	aiMesh* pMesh = inScene->mMeshes[triMeshIdx];

	// Copy vertices
	VectorSet vertices;

	std::pair<VectorSet::iterator, bool> insertResult;
	std::vector<VectorSet::iterator> indexToSetIterator;
	std::vector<int> indexMap;

	//NB: do this twice, iterators move around on insert
	for (int i = 0; i < pMesh->mNumVertices; i++)
	{
		vertices.insert(pMesh->mVertices[i]);
	}

	for (int i = 0; i < pMesh->mNumVertices; i++)
	{
		insertResult = vertices.insert(pMesh->mVertices[i]);
		indexMap.push_back(std::distance(vertices.begin(), insertResult.first));
		indexToSetIterator.push_back(insertResult.first);
	}

	// Create set iterator to index mapping
	outMesh->vertices.reserve(vertices.size());
	VectorSet::iterator first = vertices.begin(), last = vertices.end();
	for (VectorSet::iterator iter = first; iter != last; ++iter)
	{
		outMesh->vertices.push_back(Eigen::Vector3f(iter->x, iter->y, iter->z) * SCALE);
	}

	assert(NULL != pMesh->mFaces);
	outMesh->faces.resize(pMesh->mNumFaces);

	assert(pMesh->HasNormals());
	// Copy Faces (lists of vertex indices)
	for (int i = 0; i < pMesh->mNumFaces; ++i)
	{
		if (3 != pMesh->mFaces[i].mNumIndices)
		{
			std::cerr << "WTF?" << std::endl;
			continue;
		}

		Eigen::Vector3f c = Eigen::Vector3f::Zero();
		for (int j = 0; j < 3; ++j)
		{
			uint originalIdx = pMesh->mFaces[i].mIndices[j];
			int shiftedIdx = std::distance(vertices.begin(), indexToSetIterator[originalIdx]);

			outMesh->faces[i].vertexIndices[j] = shiftedIdx;
			c += outMesh->vertices[shiftedIdx];
		}

		// NB: Normals are generated per-vertex, not per face...
		aiVector3D normal = pMesh->mNormals[pMesh->mFaces[i].mIndices[0]].Normalize();
		outMesh->faces[i].normal = Eigen::Vector3f(normal.x, normal.y, normal.z);
		outMesh->faces[i].center = c / 3.0;
	}

	return true;
}

// Loop through all face pairings and check for ones that share 2 vertices
void getFaceAdjacencyGraph(Mesh* mesh, std::map<IdxPair, Edge >& strongEdges, std::map<IdxPair, Edge >& allEdges)
{
	const int numFaces = mesh->faces.size();
	const int numVertices = mesh->vertices.size();
	std::vector<std::vector<uint> > verticesToFaces(numVertices);
	for (int i = 0; i < numFaces; ++i)
	{
		Mesh::Face& face = mesh->faces[i];
		for (int m = 0; m < 3; ++m)
		{
			verticesToFaces[face.vertexIndices[m]].push_back(i);
		}
	}

//	std::map<IdxPair, Edge > strongEdges; // Face A, Face B; Vertex A, Vertex G

	int sharedEdges = 0;
	for (int v = 0; v < numVertices; ++v)
	{
		const int numAdjacentFaces = verticesToFaces[v].size();
		for (int i = 0; i < numAdjacentFaces; ++i)
		{
			Mesh::Face& faceA = mesh->faces[verticesToFaces[v][i]];
			for (int j = i+1; j < numAdjacentFaces; ++j)
			{
				Mesh::Face& faceB = mesh->faces[verticesToFaces[v][j]];

				std::vector<uint> intersection;
				intersection.reserve(2);

				for (int m = 0; m < 3; ++m)
				{
					for (int n = 0; n < 3; ++n)
					{
						if (faceA.vertexIndices[m] == faceB.vertexIndices[n])
						{
							intersection.push_back(faceA.vertexIndices[m]);
						}
					}
				}
				if (2 == intersection.size())
				{
					int a = verticesToFaces[v][i];
					int b = verticesToFaces[v][j];

//					// Always add to all edges
//					if (a < b)
//					{
//						allEdges.insert(std::pair<IdxPair, Edge >( IdxPair(a, b),
//									 Edge(intersection[0], intersection[1], a, b)));
//					}
//					else
//					{
//						allEdges.insert(std::pair<IdxPair, Edge >( IdxPair(b, a),
//										 Edge(intersection[0], intersection[1], b, a)));
//					}

					// Check normals to determine if sharp edge
					float dotProduct = faceA.normal.dot(faceB.normal);
					if (dotProduct > 1.0 && dotProduct < 1.001) { dotProduct = 1.0; }
					float angle = acosf(dotProduct);

					assert(angle == angle);
					assert(angle <= M_PI);

					if (angle < M_PI / 8.0)
					{
						continue;
					}

					if (a < b)
					{
						strongEdges.insert(std::pair<IdxPair, Edge >( IdxPair(a, b),
									 Edge(intersection[0], intersection[1], a, b)));
					}
					else
					{
						strongEdges.insert(std::pair<IdxPair, Edge >( IdxPair(b, a),
										 Edge(intersection[0], intersection[1], b, a)));
					}
				}
			}
		}
	}

//	std::cout << strongEdges.size() << std::endl;
//	return strongEdges;
}

RenderMesh::RenderMesh(const std::string filename)
{
	Assimp::Importer importer;
	const int postprocessingFlags =
			aiProcess_JoinIdenticalVertices |
			aiProcess_GenNormals |
			aiProcess_ImproveCacheLocality |
			aiProcess_Triangulate |
			aiProcess_OptimizeGraph |
			aiProcess_OptimizeMeshes |
			//aiProcess_FindDegenerates |
			aiProcess_FixInfacingNormals |
			aiProcess_SortByPType;

	const aiScene* pScene = importer.ReadFile(filename.c_str(),
											  aiProcess_ValidateDataStructure |
											  postprocessingFlags);

	if (pScene == NULL)
	{
		std::cerr << importer.GetErrorString() << std::endl;
		return;
	}

	if (!meshAItoAP(pScene, &m_mesh))
	{
		std::cerr << "Failed to load mesh: " << filename << std::endl;
		return;
	}

	std::map<IdxPair, Edge > allEdges, strongEdges;
	getFaceAdjacencyGraph(&m_mesh, strongEdges, allEdges);

	const int numFaces = m_mesh.faces.size();
	m_triangleBuffer.resize(numFaces * 3 * 3);

	for (int i = 0; i < numFaces; ++i)
	{
		for (int j = 0; j < 3; ++j)
		{
			m_triangleBuffer[((i * 3) + j) * 3 + 0] = m_mesh.vertices[m_mesh.faces[i].vertexIndices[j]].x() * SCALE;
			m_triangleBuffer[((i * 3) + j) * 3 + 1] = m_mesh.vertices[m_mesh.faces[i].vertexIndices[j]].y() * SCALE;
			m_triangleBuffer[((i * 3) + j) * 3 + 2] = m_mesh.vertices[m_mesh.faces[i].vertexIndices[j]].z() * SCALE;
		}
	}	

	glGenVertexArrays(1, &m_VAO_T);
	glBindVertexArray(m_VAO_T);
	{
		glGenBuffers(1, &m_bufferNameT);
		glBindBuffer(GL_ARRAY_BUFFER, m_bufferNameT);
		glBufferData(GL_ARRAY_BUFFER, sizeof(m_triangleBuffer[0]) * m_triangleBuffer.size(), &m_triangleBuffer[0], GL_STATIC_DRAW);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	}
	glBindVertexArray(0);

	// Generate strong edge buffer
	m_edges.reserve(strongEdges.size());
	m_edgeBuffer.reserve(strongEdges.size());
	for (std::pair<IdxPair, Edge > edgePair : strongEdges)
	{
		const Edge& edge = edgePair.second;
		int u = edge.vertexIdxA;
		int v = edge.vertexIdxB;

		m_edges.push_back(edge);

		m_edgeBuffer.push_back(m_mesh.vertices[u].x() * SCALE);
		m_edgeBuffer.push_back(m_mesh.vertices[u].y() * SCALE);
		m_edgeBuffer.push_back(m_mesh.vertices[u].z() * SCALE);

		m_edgeBuffer.push_back(m_mesh.vertices[v].x() * SCALE);
		m_edgeBuffer.push_back(m_mesh.vertices[v].y() * SCALE);
		m_edgeBuffer.push_back(m_mesh.vertices[v].z() * SCALE);
	}

	glGenVertexArrays(1, &m_VAO_E);
	glBindVertexArray(m_VAO_E);
	{
		glGenBuffers(1, &m_bufferNameE);
		glBindBuffer(GL_ARRAY_BUFFER, m_bufferNameE);
		glBufferData(GL_ARRAY_BUFFER, sizeof(m_edgeBuffer[0]) * m_edgeBuffer.size(), &m_edgeBuffer[0], GL_STATIC_DRAW);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	}
	glBindVertexArray(0);

	// Generate all edge buffer (primarily for silhouettes
	m_allEdgeBuffer.reserve(allEdges.size());
	for (std::pair<IdxPair, Edge > edgePair : allEdges)
	{
		const Edge& edge = edgePair.second;
		int u = edge.vertexIdxA;
		int v = edge.vertexIdxB;

		m_allEdgeBuffer.push_back(m_mesh.vertices[u].x() * SCALE);
		m_allEdgeBuffer.push_back(m_mesh.vertices[u].y() * SCALE);
		m_allEdgeBuffer.push_back(m_mesh.vertices[u].z() * SCALE);

		m_allEdgeBuffer.push_back(m_mesh.vertices[v].x() * SCALE);
		m_allEdgeBuffer.push_back(m_mesh.vertices[v].y() * SCALE);
		m_allEdgeBuffer.push_back(m_mesh.vertices[v].z() * SCALE);
	}

	glGenVertexArrays(1, &m_VAO_AE);
	glBindVertexArray(m_VAO_AE);
	{
		glGenBuffers(1, &m_bufferNameAE);
		glBindBuffer(GL_ARRAY_BUFFER, m_bufferNameAE);
		glBufferData(GL_ARRAY_BUFFER, sizeof(m_allEdgeBuffer[0]) * m_allEdgeBuffer.size(), &m_allEdgeBuffer[0], GL_STATIC_DRAW);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	}
	glBindVertexArray(0);

	//std::cout << m_mesh << std::endl;
	GLint err = glGetError(); if (err != GL_NO_ERROR) { std::cerr << "OpenGL error " << err << gluErrorString(err) << std::endl; }
}

void RenderMesh::Render()
{
	// Draw Faces
	const int numFaces = m_mesh.faces.size();
//	glColor3f(1, 1, 1);
	glColor3f(0,0,0);
	glBegin(GL_TRIANGLES);
	for (int i = 0; i < numFaces; ++i)
	{
		if (m_visibleFaces[i])
		{
			for (int j = 0; j < 3; ++j)
			{
				Eigen::Vector3f& v = m_mesh.vertices[m_mesh.faces[i].vertexIndices[j]];
				glVertex3f( v.x() * SCALE,
							v.y() * SCALE,
							v.z() * SCALE);
			}
		}
	}
	glEnd();


	// Draw Edges
	glLineWidth(2.0);
	//const int numEdgeVertices = m_edgeBuffer.size() / 3;
	const int numEdges = m_edges.size();
	glColor3f(1, 0, 0);
	glBegin(GL_LINES);
	for (int i = 0; i < numEdges; ++i)
	{
		if (m_visibleEdges[i])
		{
			const Edge& e = m_edges[i];
			Eigen::Vector3f& va = m_mesh.vertices[e.vertexIdxA];
			Eigen::Vector3f& vb = m_mesh.vertices[e.vertexIdxB];

			glVertex3f(va.x() * SCALE, va.y() * SCALE, va.z() * SCALE);
			glVertex3f(vb.x() * SCALE, vb.y() * SCALE, vb.z() * SCALE);

		}
	}
	glEnd();
	glLineWidth(1.0);


	// Draw Normals
	glColor3f(0, 0, 1);
	glBegin(GL_LINES);
	for (int i = 0; i < numFaces; ++i)
	{
		if (m_visibleFaces[i])
		{
			// Get center
			Eigen::Vector3f c = m_mesh.faces[i].center * SCALE;
			glVertex3f( c.x(),
						c.y(),
						c.z());

			// Get endpoint
			Eigen::Vector3f e = c + (m_mesh.faces[i].normal * SCALE / 30.0);
			glVertex3f( e.x(),
						e.y(),
						e.z());
		}

	}
	glEnd();


}

void RenderMesh::RenderFaces() const
{
	//glColor3f(0,0,0);
	glBindVertexArray(m_VAO_T);
	{
		glDrawArrays(GL_TRIANGLES, 0, m_triangleBuffer.size()/3);
	}
	glBindVertexArray(0);
}

void RenderMesh::RenderEdges() const
{
	glColor3f(1,1,1);
	glLineWidth(2.0);
	glBindVertexArray(m_VAO_E);
	{
		glDrawArrays(GL_LINES, 0, m_edgeBuffer.size()/2);
	}
	glBindVertexArray(0);
	glLineWidth(1.0);
}

void RenderMesh::RenderArray()
{
	glPushAttrib(GL_ALL_ATTRIB_BITS);

	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	glDisable(GL_TEXTURE_2D);


	glEnable( GL_LIGHTING );
	// Set the clear value for the stencil buffer, then clear it
	glClearStencil(0);
	glClear( GL_STENCIL_BUFFER_BIT );
	glEnable( GL_STENCIL_TEST );
	// Set the stencil buffer to write a 1 in every time
	// a pixel is written to the screen
	glStencilFunc( GL_ALWAYS, 1, 0xFFFF );
	glStencilOp( GL_KEEP, GL_KEEP, GL_REPLACE );
	// Render the object in black
	glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
	glColor3f( 0.0f, 0.0f, 0.0f );
	RenderFaces();
	glDisable( GL_LIGHTING );
	// Set the stencil buffer to only allow writing
	// to the screen when the value of the
	// stencil buffer is not 1
	glStencilFunc( GL_NOTEQUAL, 1, 0xFFFF );
	glStencilOp( GL_KEEP, GL_KEEP, GL_REPLACE );
	// Draw the object with thick lines
	glLineWidth( 4.0f );
	glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
	glColor3f( 1.0f, 1.0f, 1.0f );
	RenderFaces();

	glClearStencil(0);
	glClear(GL_STENCIL_BUFFER_BIT);
	RenderEdges();

	glPopAttrib();
}


void RenderMesh::ComputeVisibility(Eigen::Vector3f& cameraOrigin)
{
	const int numFaces = m_mesh.faces.size();
	const int numEdges = m_edges.size();

	if (m_visibleFaces.size() != numFaces)
	{ m_visibleFaces.resize(numFaces, false); }
	if (m_visibleEdges.size() != numEdges)
	{ m_visibleEdges.resize(numEdges, false); }

	for (int i = 0; i < numFaces; ++i)
	{
		const Mesh::Face& face = m_mesh.faces[i];

		Eigen::Vector3f cameraZ = face.center - cameraOrigin;
		float dotProduct = face.normal.dot(cameraZ);
		//m_visibleFaces[i] = dotProduct <= 0; // Normal faces towards camera, so the dot is negative
		m_visibleFaces[i] = dotProduct <= 0.0;
	}

	for (int i = 0; i < numEdges; ++i)
	{
		const Edge& e = m_edges[i];

		m_visibleEdges[i] = m_visibleFaces[e.faceIdxA] || m_visibleFaces[e.faceIdxB];
	}
}


std::ostream& operator <<(std::ostream& s, Mesh r)
{
	s << "Points:  " << std::endl;
	for (int i = 0; i < r.vertices.size(); ++i)
		s << "\t" << i << ":   " << r.vertices[i].transpose() << std::endl;
	s << "Faces:  " << std::endl;
	for (int i = 0; i < r.faces.size(); ++i)
	{
		s << "\t" << i << ":  " << r.faces[i].vertexIndices[0] << ","
								<< r.faces[i].vertexIndices[1] << ","
								<< r.faces[i].vertexIndices[2] << std::endl;
	}
	return s;
}


void Mesh::boundingBox(float& xMin, float& yMin, float& zMin, float& xMax, float& yMax, float& zMax)
{
	xMin = 1e9; yMin = 1e9; zMin = 1e9;
	xMax = 0; yMax = 0; zMax = 0;
	const int numVertices = this->vertices.size();
	for (int i = 0; i < numVertices; ++i)
	{
		const Eigen::Vector3f v = this->vertices[i];
		if (v.x() < xMin) { xMin = v.x(); }
		if (v.x() > xMax) { xMax = v.x(); }
		if (v.y() < yMin) { yMin = v.y(); }
		if (v.y() > yMax) { yMax = v.y(); }
		if (v.z() < zMin) { zMin = v.z(); }
		if (v.z() > zMax) { zMax = v.z(); }
	}
}

void Mesh::boundingSphere(float& x, float& y, float& z, float& r)
{
	float xMin,  yMin,  zMin,  xMax,  yMax,  zMax;
	this->boundingBox(xMin,  yMin,  zMin,  xMax,  yMax,  zMax);
	x = (xMax + xMin) / 2.0f;
	y = (yMax + yMin) / 2.0f;
	z = (zMax + zMin) / 2.0f;

	float dX, dY, dZ;
	dX = xMax - xMin;
	dY = yMax - yMin;
	dZ = zMax - zMin;

	r = sqrt(dX*dX + dY*dY + dZ*dZ) / 2.0f;
}
