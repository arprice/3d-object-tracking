/**
 * \file RayProjector.h
 * \brief
 *
 * \author Andrew Price
 * \date 7 7, 2014
 *
 * \copyright
 *
 * Copyright (c) 2014, Georgia Tech Research Corporation
 * All rights reserved.
 *
 * This file is provided under the following "BSD-style" License:
 * Redistribution and use in source and binary forms, with or
 * without modification, are permitted provided that the following
 * conditions are met:
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef RAYPROJECTOR_H
#define RAYPROJECTOR_H

#include <Eigen/Geometry>

#include "CommonDefinitions.h"

class RayProjector
{
public:
	static RayProjector* Instance();

	/**
	 * @brief Computes ray from origin through pixel
	 * @param u Image coordinate column
	 * @param v Image coordinate row
	 * @return Normalized ray
	 *
	 * Image coordinates:
	 *      u->
	 * (0,0)____________
	 *   v |          |
	 *   | |          |
	 *   V |          |
	 *     |          |
	 *     |__________|
	 *
	 * CG coordinates:
	 *    y
	 *    |
	 *    |___ x
	 *   /
	 *  z
	 *
	 * CV coordinates:
	 *     z
	 *	  /___ x
	 *	  |
	 *	  |
	 *    y
	 */
	static inline Eigen::Vector3f rayThroughPixel(const int u, const int v)
	{
		assert(NULL != this->mpInstance);
		return rays[v][u];
	}

	static Eigen::Vector3f rays[RENDER_SIZE_Y][RENDER_SIZE_X];

private:
	static RayProjector* mpInstance;
	RayProjector();
	RayProjector(RayProjector const&) {}
#pragma warning( push, 0 )
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wreturn-type"
	RayProjector& operator=(RayProjector const&) {}
#pragma GCC diagnostic pop
#pragma warning( pop )
};


#endif // RAYPROJECTOR_H
