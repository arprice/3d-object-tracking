/**
 * \file CommonDefinitions.h
 * \brief
 *
 * \author Andrew Price
 * \date 7 7, 2014
 *
 * \copyright
 *
 * Copyright (c) 2014, Georgia Tech Research Corporation
 * All rights reserved.
 *
 * This file is provided under the following "BSD-style" License:
 * Redistribution and use in source and binary forms, with or
 * without modification, are permitted provided that the following
 * conditions are met:
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef COMMONDEFINITIONS_H
#define COMMONDEFINITIONS_H

#ifndef NDEBUG
#   define assert_msg(condition, message) \
	do { \
		if (! (condition)) { \
			std::cerr << "Assertion `" #condition "` failed in " << __FILE__ \
					  << " line " << __LINE__ << ": " << message << std::endl; \
			std::exit(EXIT_FAILURE); \
		} \
	} while (false)
#else
#   define assert_msg(condition, message) do { } while (false)
#endif

//const std::string cannyFilename = "/home/arprice/Dropbox/GTRI/sample_images/gears.png";
//const std::string modelFilename = "/home/arprice/Dropbox/GTRI/VEX-60-TOOTH-GEAR.STL";
const std::string cannyFilename = "/home/arprice/Desktop/cube.png";
const std::string modelFilename = "/home/arprice/Desktop/cube.stl";

const int RENDER_SIZE_X = 240;//480;
const int RENDER_SIZE_Y = 240;

const float FOV_DEG = 15.0f; //45.0f;
const float FOV = FOV_DEG * M_PI / 180.0;

const float FOV_DELTA_X = FOV / (float)RENDER_SIZE_X;
const float FOV_DELTA_Y = FOV / (float)RENDER_SIZE_Y;

#endif // COMMONDEFINITIONS_H
