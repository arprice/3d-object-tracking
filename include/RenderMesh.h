/**
 * \file RenderMesh.h
 * \brief
 *
 * \author Andrew Price
 * \date 6 26, 2014
 *
 * \copyright
 *
 * Copyright (c) 2014, Georgia Tech Research Corporation
 * All rights reserved.
 *
 * This file is provided under the following "BSD-style" License:
 * Redistribution and use in source and binary forms, with or
 * without modification, are permitted provided that the following
 * conditions are met:
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef RENDERMESH_H
#define RENDERMESH_H

#include <string>
#include <vector>
#include <deque>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <boost/graph/adjacency_list.hpp>
//#include <boost/serialization/string.hpp>
//#include <boost/graph/adj_list_serialize.hpp>
//#include <boost/graph/filtered_graph.hpp>

typedef boost::adjacency_list<boost::setS, boost::vecS, boost::undirectedS, boost::no_property, boost::no_property> FaceGraph;

struct Edge
{
public:
	Edge(int va, int vb, int fa, int fb) { vertexIdxA=va; vertexIdxB=vb; faceIdxA=fa; faceIdxB=fb; }
	int vertexIdxA;
	int vertexIdxB;
	int faceIdxA;
	int faceIdxB;
	bool operator< (const Edge& other);
};

class Mesh
{
public:

	class Face
	{
	public:
		Face() {}
		Face(int a, int b, int c)
		{
			vertexIndices[0] = a;
			vertexIndices[1] = b;
			vertexIndices[2] = c;
		}

		unsigned int vertexIndices[3];

		Eigen::Vector3f normal;
		Eigen::Vector3f center;
	};

	std::vector<Eigen::Vector3f, Eigen::aligned_allocator<Eigen::Vector3f> > vertices;
	std::vector<Face> faces;

	void boundingBox(float& xMin, float& yMin, float& zMin, float& xMax, float& yMax, float& zMax);
	void boundingSphere(float& x, float& y, float& z, float& r);

	//float volume() const;
};

class RenderMesh
{
public:
	RenderMesh(const std::string filename);

	void Render();
	void RenderArray();

	void ComputeVisibility(Eigen::Vector3f& cameraZ);

	Mesh m_mesh;

	std::vector<Edge> m_edges;
	std::vector<bool> m_visibleFaces;
	std::vector<bool> m_visibleEdges;

	std::vector<float> m_triangleBuffer;
	std::vector<float> m_edgeBuffer;
	std::vector<float> m_allEdgeBuffer;

	unsigned int m_VAO_T;
	unsigned int m_VAO_E;
	unsigned int m_VAO_AE;
	unsigned int m_bufferNameT;
	unsigned int m_bufferNameE;
	unsigned int m_bufferNameAE;

	int m_colorHandle;

private:
	void RenderFaces() const;
	void RenderEdges() const;

	//FaceGraph m_faceGraph;
};

std::ostream& operator <<(std::ostream& s, Mesh r);

#endif // RENDERMESH_H
