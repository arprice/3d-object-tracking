/**
 * \file RenderMatches.h
 * \brief
 *
 * \author Andrew Price
 * \date 7 4, 2014
 *
 * \copyright
 *
 * Copyright (c) 2014, Georgia Tech Research Corporation
 * All rights reserved.
 *
 * This file is provided under the following "BSD-style" License:
 * Redistribution and use in source and binary forms, with or
 * without modification, are permitted provided that the following
 * conditions are met:
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef RENDERMATCHES_H
#define RENDERMATCHES_H

#include <opencv2/core/core.hpp>
#include <Eigen/Geometry>

#include "CommonDefinitions.h"

class OrbitParams
{
public:
	OrbitParams(float r=1, float t=0, float p=M_PI) { rho=r; theta=t; phi=p; }
	float rho, theta, phi;

	Eigen::Vector3f toCartesian() const
	{
		return Eigen::Vector3f(rho*cos(theta)*cos(phi),
							   rho*cos(theta)*sin(phi),
							   rho*sin(theta));
	}

	Eigen::Vector3f toCGCartesian() const
	{
		return Eigen::Vector3f(rho*cos(theta)*cos(phi),
							   rho*sin(theta),
							   rho*cos(theta)*sin(phi));
	}
};

void renderMatches();

void draw3DMatches(const std::vector<cv::Point2i>& realEdgeIndices,
				   const std::vector<cv::Point2i>& synthEdgeIndices,
				   const cv::Mat& coordImg,
				   const cv::Mat& matchIndices,
				   const Eigen::Isometry3f& objectOrigin,
				   const OrbitParams op = OrbitParams());

#endif // RENDERMATCHES_H
